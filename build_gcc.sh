#!/bin/bash

set -e

pushd .
export PREFIX="$PWD/install-prefix"
export TARGET=propeller-elf
export PATH="$PREFIX/bin:$PATH"

rm -rf build-gcc
mkdir build-gcc
cd build-gcc
$PWD/../gcc/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers --enable-checking
make all-gcc -j `nproc --all`
#make all-target-libgcc -j `nproc --all`
make install-gcc
#make install-target-libgcc

popd
