#!/bin/bash

set -e

pushd .
export PREFIX="$PWD/install-prefix"
export TARGET=propeller-elf
export PATH="$PREFIX/bin:$PATH"

rm -rf build-binutils
mkdir build-binutils
cd build-binutils
$PWD/../binutils-gdb/configure --target=$TARGET --prefix="$PREFIX" -with-sysroot --disable-nls --disable-werror
make -j `nproc --all`
make install

popd
